#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Creator:          D. Herre
GitLab:      dikon/afh-gnd

Created:        2020-04-30
Last Modified:  2020-06-19
"""

from librair.parsers import Beacon
from librair.schemas import json

BEACON = "https://dikon.gitlab.io/cph-beacon/data/gnd.txt"
HREF = "edits/index1694-href.txt"
NAMES = "edits/index1694-names.txt"
OUT = "data/index1694.json"

href = []
with open(HREF, 'r', encoding="utf-8") as f:
    href = [l.strip() for l in f.readlines()]

names = []
with open(NAMES, 'r', encoding="utf-8") as f:
    names = [l.strip() for l in f.readlines()]

cph = Beacon(url=BEACON)

gnds = []
targets = [t[1] for t in cph.targets]
additions = {
  "morgenstern-nikolaus": "104171421",
  "reichhelm-karl": "1020385375",
  "sperlette-bartholomaeus-johann": "1042343306"
}

for h in href:
    if h not in targets and h not in additions:
        print("no GND identifier present for", h)

for i, target in enumerate(cph.targets):
    if target[1] in href:
        gnds.append(cph.links[i])

for person in additions:
    gnds.append(additions[person])

json.writer(gnds, OUT)

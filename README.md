# GND-Nummern von Professoren der Academia Fridericiana Halensis

Auf Basis der [BEACON-Datei](./../../../../cph-beacon/) des [Catalogus Professorum Halensis](https://www.catalogus-professorum-halensis.de/) und der [HTML-Indexseite](https://www.catalogus-professorum-halensis.de/index1694.html) des Katalogabschnitts zur ersten Etappe in der Geschichte der hallenser Universität (1694–1817) wurden für alle Professoren  – soweit vorhanden – korrespondierende Normnummern ermittelt und in einer hier dokumentierten [JSON-Datei](./data/index1694.json) zusammengefasst. Von den insgesamt 187 Professoren, die in diesem Zeitraum gelehrt haben, konnten auf diese Weise 181 eindeutig identifiziert werden.

Dass die Epoche der Aufklärung im Professorenkatalog überhaupt abgedeckt ist, verdankt sich den jüngeren Studien Julia Schopferers zur _Sozialgeschichte der halleschen Professoren 1694–1806_ [1]. Sie hat ihre „Quellen zu den Untersuchungspersonen ... umfassend gesammelt, elektronisch aufgenommen ... und in eine Datenbank eingefügt“ (p. 36). Auf dieser Basis sind die Einträge für den besagten Zeitraum im Professorenkatalog entstanden.

---

[1] Julia Schopferer, _Sozialgeschichte der halleschen Professoren 1694–1806: Lebenswege, Netzwerke und Raum als Strukturbedingungen von universitärer Wissenschaft und frühmoderner Gelehrtenexistenz_, Halle/Saale: Mitteldeutscher Verlag 2016.
